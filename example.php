<?php
require_once('API.php');
$fapi = new FroxlorAPI();
/*
* 1st Param -> Username
* 2nd Param -> Password
*
*/
$fapi->SetUserdata('APITestuser', 'ABCdef123');
if(!$fapi->Login())
{
    echo 'Login NOK<br />';
}
else
{
    echo 'Login OK<br />';
	/*
	 * 1st Param -> Name
	 * 2nd Param -> First Name
	 * 3rd Param -> Company Name
	 * 4th Param -> Mail Address
	 *
	 */
    $fapi->SetPersonaldata('Testuser', 'Number 1', '', 'nobody@nowhere.xyz');
	/*
	 * 1st Param -> Webspace (GiB)
	 * 2nd Param -> Traffic (Gib)
	 * 3rd Param -> Number of Subdomains
	 * 4th Param -> Number of mail addresses
	 * 5th Param -> Number of mail accounts
	 * 6th Param -> Number of mail forwarders
	 * 7th Param -> IMAP enabled
	 * 8th Param -> POP enabled
	 * 9th Param -> Number of FTP-Accounts
	 * 10th Param -> Number of databases
	 * 11th Param -> (optional) Create default subdomain (default = 0)
	 * 12th Param -> (optional) PHP enabled (default = true)
	 * 13th Param -> (optional) Pearl enabled (default = false)
	 */
    $fapi->SetWebspacedata(1,1,1,1,1,1,1,1,1,1);
    if($fapi->CreateAccount())
	{
		echo 'Webspaceaccount successfully created';
	}
	else
	{
		echo 'Webspaceaccount could not be created';
	}
}
?>