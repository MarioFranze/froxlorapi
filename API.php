<?php
/**
 * 
 * Inofficial Froxlor API by www.CloudCoding.de
 * Supported by www.myvirtualserver.com
 * 
 * Developement Version, still in Process!
 * 
 */
class FroxlorAPI
{
	//Put here all information below
	private $_froxlorAPIAddress = ''; //i.e. http://somdomain.ie/froxlor/ - The last '/' is important!
	private $_froxlorAPIUsername = '';
	private $_froxlorAPIPassword = '';
	//End of needed information
	
	private $_froxlorAPISession = '';
	private $_request;
	private $_requestHandler;
	private $_requestHeader = array();
	private $_debug = true;
	
	
	//-- User Data
	private $_newUsername = '';
	private $_newPassword = '';
	private $_sendPassword = true;
	private $_createIndexFile = true;
	
	private $_setUserdata = false;
	
	//-- Personal Data
	private $_newName = '';
	private $_newFirstName = '';
	private $_newGender = '';
	private $_newCompany = '';
	private $_newStreet = '';
	private $_newZipcode = '';
	private $_newCity = '';
	private $_newPhone = '';
	private $_newFax = '';
	private $_newMail = ''; 
	   
	private $_setPersonaldata = false;
	
	//-- Webspacedata
	private $_newWebspace = 0;
	private $_newTraffic = 0;
	private $_newSubdomains = 0;
	private $_newMailAddresses = 0;
	private $_newMailAccounts = 0;
	private $_newMailForwarders = 0;
	private $_newMailImap = true;
	private $_newMailPop = true;
	private $_newFtpAccounts = 0;
	private $_newDatabases = 0;
	private $_newPhpEnabled = true;
	private $_newPealEnabled = false;
	private $_newCreateStdSubdomain = 0;
	
	private $_setHostingdata = false;
	
	function __construct()
	{
		if (!function_exists('curl_init'))
		{
			die('Please contact the Webmaster for further Assistance. ERROR: cURL not installed');
		}
		$this->_requestHandler = curl_init();
		curl_setopt($this->_requestHandler, CURLOPT_USERAGENT, 'FroxlorRequestAPI');
		curl_setopt($this->_requestHandler, CURLOPT_HEADER, 1);
		curl_setopt($this->_requestHandler, CURLOPT_RETURNTRANSFER, true);
	}
	
	function __destruct()
	{
		$this->ResetToDeafault();
		//TODO: Free Resources
	}
	
	/**
	 * 
	 * Perform the Login on the given Server
	 * 
	 */
	function Login()
	{
		curl_setopt($this->_requestHandler, CURLOPT_URL, $this->_froxlorAPIAddress . 'index.php');
		curl_setopt($this->_requestHandler, CURLOPT_POST, 1);
		curl_setopt($this->_requestHandler, CURLOPT_POSTFIELDS, 'loginname=' . $this->_froxlorAPIUsername . '&password=' . $this->_froxlorAPIPassword	. '&send=send');
		$this->_request = curl_exec($this->_requestHandler);
		if(!$this->ParseHeader())
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	
	/**
	 * 
	 * Set the userdata for the accountcreation via API
	 * @param string	$username		Unique username for the Backend
	 * @param string	$password		A password containing letters (uppercase and lowercase) and numbers
	 * @param bool		$sendpassword	Should the password sent to the customer (optional - default yes)
	 * @param bool		$indexfile		Should there be a default index site (optional - default yes)
	 * 
	 */
	function SetUserdata($username, $password, $sendpassword = true, $indexfile = true)
	{
		$this->_newUsername = $username;
		$this->_newPassword = $password;
		$this->_sendPassword = $sendpassword;
		$this->_createIndexFile = $indexfile;
		
		$this->_setUserdata = true;
	}
	
	/**
	 * 
	 * Set the personal data for the new account via API
	 * @param string	$name		Prename of the new Account
	 * @param string	$firstname	First name of the new Account
	 * @param string	$company	Alternativ -> Company-Name of the new Account
	 * @param string	$mail		E-Mail address of the new Account
	 * @param string	$address	(optional) Name of the Street
	 * @param int		$zip		(optional) ZIP Code
	 * @param string	$city		(optional) Name of the City
	 * @param int		$gender		(optional) Gender - 0 -> None | 1 -> Male | 2 -> Female 
	 * @param int		$phone		(optional) Phonenumber
	 * @param int		$fax		(optional) Fax Number
	 */	 
	function SetPersonaldata($name, $firstname, $company, $mail, $address = '', $zip = '', $city = '', $gender = '', $phone = '', $fax = '')
	{
		$this->_newName = $name;
		$this->_newFirstName = $firstname;
		$this->_newCompany = $company;
		$this->_newMail = $mail;
		$this->_newStreet = $address;
		$this->_newZipcode = $zip;
		$this->_newCity = $city;
		$this->_newGender = $gender;
		$this->_newPhone = $phone;
		$this->_newFax = $fax;
		
		$this->_setPersonaldata = true;
	}
	
	/**
	 * 
	 * Set the hostingsettings for the new account via API
	 * @param int	$webspace			Space in GiB
	 * @param int	$traffic			Traffic in GiB
	 * @param int	$subdomains			Number of Subdomains
	 * @param int	$mailaddresses		Number of E-Mail Adresses
	 * @param int	$mailaccounts		Number of E-Mail Accounts
	 * @param int	$mailforwarders		Number of E-Mail Forwarders
	 * @param bool	$mailimap			IMAP enabled
	 * @param bool	$mailpop			POP enabled
	 * @param int	$ftpaccounts		Number of FTP Accounts
	 * @param int	$databases			Number of Databases
	 * @param int	$createstdsubdomain	(optional) Create a default domain
	 * @param bool	$phpenabled			(optional) PHP enabled (defaul yes)
	 * @param bool	$pealenabled		(optional) Peal enabled (default no)
	 * 
	 */
	function SetWebspacedata($webspace, $traffic, $subdomains, $mailaddresses, $mailaccounts, $mailforwarders, $mailimap, $mailpop, $ftpaccounts, $databases, $createstdsubdomain = 0, $phpenabled = true, $pealenabled = false)
	{
		$this->_newWebspace = $webspace;
		$this->_newTraffic = $traffic;
		$this->_newSubdomains = $subdomains;
		$this->_newMailAccounts = $mailaccounts;
		$this->_newMailAddresses = $mailaddresses;
		$this->_newMailForwarders = $mailforwarders;
		$this->_newMailImap = $mailimap;
		$this->_newMailPop = $mailpop;
		$this->_newFtpAccounts = $ftpaccounts;
		$this->_newDatabases = $databases;
		$this->_newPhpEnabled = $phpenabled;
		$this->_newPealEnabled = $pealenabled;
		$this->_newCreateStdSubdomain = $createstdsubdomain;
		
		$this->_setHostingdata = true;
	}
	
	/**
	 * 
	 * Creates the customeraccount with the given data
	 * 
	 */
	function CreateAccount()
	{
		if($this->_setUserdata == false)
		{
			if($this->_debug)
			{ 
				echo 'Please contact the Webmaster for further Assistance. ERROR: SetUserdata() is never called!';				
		   	}
			return false;
		}
		else if($this->_setPersonaldata == false)
		{
			if($this->_debug)
			{ 
				echo 'Please contact the Webmaster for further Assistance. ERROR: SetPersonaldata() is never called!';				
		   	}
			return false;
		}
		else if($this->_setHostingdata == false)
		{
			if($this->_debug)
			{ 
				echo 'Please contact the Webmaster for further Assistance. ERROR: SetWebspacedata() is never called!';				
		   	}
			return false;
		}
		else
		{
			$postfields  = 'send=send&page=customers&action=add&s=' . $this->_froxlorAPISession . '&createstdsubdomain=' . $this->_newCreateStdSubdomain;
			$postfields .= '&customernumber=&def_language=&custom_notes=&tickets=';
			$postfields .= '&new_loginname=' . $this->_newUsername . '&store_defaultindex=' . $this->_createIndexFile;
			$postfields .= '&new_customer_password=' . $this->_newPassword . '&sendpassword=' . $this->_sendPassword;
			$postfields .= '&name=' . $this->_newName . '&firstname=' . $this->_newFirstName . '&gender=' . $this->_newGender;
			$postfields .= '&company=' . $this->_newCompany . '&street=' . $this->_newStreet . '&zipcode=' . $this->_newZipcode;
			$postfields .= '&city=' . $this->_newCity . '&phone=' . $this->_newPhone . '&fax=' . $this->_newFax;
			$postfields .= '&email=' . $this->_newMail . '&diskspace=' . $this->_newWebspace . '&traffic=' . $this->_newTraffic;
			$postfields .= '&subdomains=' . $this->_newSubdomains . '&emails=' . $this->_newMailAddresses;
			$postfields .= '&email_accounts=' . $this->_newMailAccounts . '&email_forwarders=' . $this->_newMailForwarders;
			$postfields .= '&email_imap=' . $this->_newMailImap . '&email_pop3=' . $this->_newMailPop;
			$postfields .= '&ftps=' . $this->_newFtpAccounts . '&mysqls=' . $this->_newDatabases;
			$postfields .= '&phpenabled=' . $this->_newPhpEnabled . '&perlenabled=' . $this->_newPealEnabled;
			
			curl_setopt($this->_requestHandler, CURLOPT_URL, $this->_froxlorAPIAddress . 'admin_customers.php?s=' . $this->_froxlorAPISession);
			curl_setopt($this->_requestHandler, CURLOPT_POST, 1);
			curl_setopt($this->_requestHandler, CURLOPT_POSTFIELDS, $postfields);
			$this->_request = curl_exec($this->_requestHandler);

			if((int)strpos($this->_request, '<!DOCTYPE html>') == 0)
			{
				return true;
			}
			else
			{
				if($this->_debug)
				{
					  $start = (int) strpos($this->_request, '<div class="error">') + 19;
					  $end = (int) strpos($this->_request, '</div>', $start);
					  $errormsg = substr($this->_request, $start, $end - $start);
					  echo $errormsg;
		   		}
				return false;
			}
		}
	}
	
	/*
	function GetUserIDByUsername($username)
	{
		curl_setopt($this->_requestHandler, CURLOPT_URL, $this->_froxlorAPIAddress . 'admin_domains.php?s=' . $this->_froxlorAPISession . '&page=domains&action=add');
		curl_setopt($this->_requestHandler, CURLOPT_POST, 0);
		$this->_request = curl_exec($this->_requestHandler);
		$matches = preg_match("#<select id=\"customerid\" name=\"customerid\">(.*)</select>#", $this->_request, $matches);
		print_r($matches);
	}
	*/
	
	private function ParseHeader()
	{
		if(empty($this->_requestHandler))
		{
			if($this->_debug)
			{						
				echo 'Please contact the Webmaster for further Assistance. ERROR: parseHeader() - No HTTP-Header Information available';
			}			
			return false;
		}
		$splitHeader = explode(PHP_EOL, $this->_request);
		$splitHTTPCode = explode(' ', $splitHeader[0]);
		
		switch($splitHTTPCode[1])
		{
			case 302:
				for($i = 1; $i < count($splitHeader); $i++)
				{
					if(!empty($splitHeader[$i]))
					{
						if(strlen($splitHeader[$i]) > 5)
						{
							$split = explode(': ', $splitHeader[$i], 2);
							$this->_requestHeader[$split[0]] = $split[1];
						}
					}
				}
				
				if(!$this->StartsWith($this->_requestHeader['Location'], 'admin_index.php'))
				{
					if($this->_debug)
					{					
						echo 'Please contact the Webmaster for further Assistance. ERROR: parseHeader() - Wrong Froxlor Login';
					}
				}   
				else
				{
					$sessionSplit = explode('?s=', $this->_requestHeader['Location']);
					$this->_requestHeader['SID'] = substr($sessionSplit[1], 0, 32);
					$this->_froxlorAPISession = $this->_requestHeader['SID'];
					return true;
				}
				break;
			
			case 404:
				if($this->_debug)
				{					
					echo 'Please contact the Webmaster for further Assistance. ERROR: parseHeader() - Wrong Froxlor installation Path';
				}
				break;
				   
			default:
				if($this->_debug)
				{					
					echo 'Please contact the Webmaster for further Assistance. ERROR: parseHeader() - HTTP Code: ' . $splitHTTPCode[1];
				}
				break;
		}
		
		return false;
	}
	
	/**
	 * 
	 * Resets all customerdata to default
	 * 
	 */	
	private function ResetToDeafault()
	{
		//-- User Data
		$this->_newUsername = '';
		$this->_newPassword = '';
		$this->_sendPassword = true;
		$this->_createIndexFile = true;

		$_setUserdata = false;

		//-- Personal Data
		$this->_newName = '';
		$this->_newFirstName = '';
		$this->_newGender = '';
		$this->_newCompany = '';
		$this->_newStreet = '';
		$this->_newZipcode = '';
		$this->_newCity = '';
		$this->_newPhone = '';
		$this->_newFax = '';
		$this->_newMail = ''; 

		$this->_setPersonaldata = false;

		//-- Webspacedata
		$this->_newWebspace = 0;
		$this->_newTraffic = 0;
		$this->_newSubdomains = 0;
		$this->_newMailAddresses = 0;
		$this->_newMailAccounts = 0;
		$this->_newMailForwarders = 0;
		$this->_newMailImap = true;
		$this->_newMailPop = true;
		$this->_newFtpAccounts = 0;
		$this->_newDatabases = 0;
		$this->_newPhpEnabled = true;
		$this->_newPealEnabled = false;
		$this->_newCreateStdSubdomain = 0;
		
		$this->_setHostingdata = false;
	}
	
	private function StartsWith($haystack, $needle) 
	{
		return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
	}
	
	private function EndsWith($haystack, $needle) 
	{
		return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
	}
	
	private function GetTextBetweenTags($string, $tagname) 
	{
	    $pattern = "/<$tagname ?.*>(.*)<\/$tagname>/";
	    preg_match($pattern, $string, $matches);
	    return $matches;
	}
}
?>